<?php

namespace App\Classes\Eway;


use Illuminate\Support\Facades\Log;
use Eway\Rapid;
use Eway\Rapid\Enum\ApiMethod;
use Eway\Rapid\Contract\Client as ClientContract;
use GuzzleHttp\Client;
use SimpleXMLElement;


class EwayGateway
{


    public static function createClient($customerData)
    {

        $operation = 'CreateCustomer';
        $debug = true;

        $ns = 'https://www.eway.com.au/gateway/managedpayment';
        $soapAction = $ns . '/' . $operation;

        $data = "
<Title>Mr.</Title>
<FirstName>{$customerData['Customer']['FirstName']}</FirstName>
<LastName>{$customerData['Customer']['LastName']}</LastName>
<Address></Address>
<Suburb></Suburb>
<State></State>
<Company></Company>
<PostCode></PostCode>
<Country>au</Country>
<Email></Email>
<Fax></Fax>
<Phone></Phone>
<Mobile></Mobile>
<CustomerRef>dummyref</CustomerRef>
<JobDesc></JobDesc>
<Comments></Comments>
<URL></URL>
<CCNumber>{$customerData['Customer']['CardDetails']['Number']}</CCNumber>
<CCNameOnCard>{$customerData['Customer']['CardDetails']['Name']}</CCNameOnCard>
<CCExpiryMonth>{$customerData['Customer']['CardDetails']['ExpiryMonth']}</CCExpiryMonth>
<CCExpiryYear>{$customerData['Customer']['CardDetails']['ExpiryYear']}</CCExpiryYear>";

        $ewayCustomerId = env('ewayCustomerId');
        $ewayUserId = env('ewayUserId');
        $ewayPassword = env('ewayRapidPassword');

        $xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <eWAYHeader xmlns="$ns">
      <eWAYCustomerID>{$ewayCustomerId}</eWAYCustomerID>
      <Username>{$ewayUserId}</Username>
      <Password>{$ewayPassword}</Password>
    </eWAYHeader>
  </soap:Header>
  <soap:Body>
    <$operation xmlns="$ns">$data</$operation>
  </soap:Body>
</soap:Envelope>
XML;

        $client = new Client();
        try {

            $response = $client->request('POST', env('ewayRapidUrl'), ['verify' => false, 'headers' => ['SOAPAction' => $soapAction, 'content-type' => 'text/xml', 'Accept' => 'text/xml'], 'body' => $xmlRequest]);

        } catch (\Exception $exception) {
            Log::error('Eway createTokenCustomer failed with Exception: ' . $exception->getMessage());
            if ($exception->hasResponse()) {
                $error = $exception->getResponse()->getBody()->getContents();
                Log::error('Eway createTokenCustomer failed with Exception. Response: ' . $error);
                throw new \Exception("Eway createTokenCustomer failed with Exception. Response: " . $error);
            } else {
                throw new \Exception("Eway createTokenCustomer failed with Exception: " . $exception->getMessage());
            }
         }

        if ($response->getStatusCode() != 200) {
            Log::error('Eway createTokenCustomer failed with Status Code: ' . $response->getStatusCode() . " " . implode('; ', $response->getReasonPhrase()));
            throw new \Exception("Eway createTokenCustomer failed with Status Code: " . $response->getStatusCode() . " " . implode('; ', $response->getReasonPhrase()));
        }

        $result = $response->getBody()->getContents();

        $xresult = new SimpleXMLElement($result, 0, false, $ns, true);
        $xresult = $xresult->xpath('//soap:Body');
        $xresult = $xresult[0];
        $xresult = $xresult->CreateCustomerResponse->CreateCustomerResult;
        $token = $xresult->__toString();

         if (isset($token)) {
            return $token;
        } else {
            Log::error('Eway createTokenCustomer did not return a token: ' . print_r($result, true));
            throw new \Exception("Eway createTokenCustomer did not return a token: " . print_r($result, true));
        }
    }

}
