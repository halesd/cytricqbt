<?php

namespace App\Classes\QBTGateway;
/**
 * QBT Connection for Rest/JSON.
 *
 */

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

class QBTGateway
{
    const SYNC_API = '/api/PNR/SyncHotelBooking';
    const GET_API = '/api/PNR/Retrieve';

    public static function getPNRResponse($data)
    {
        $client = new Client();
        try {
            $response = $client->request('POST', env('QBTUrl') . self::GET_API, ['verify' => false, 'headers' => ['content-type' => 'application/json', 'Accept' => 'application/json'], 'body' => $data, 'auth' => [env('QBTUser'), env('QBTPassword')]]);
        } catch (\Exception $exception) {
            Log::error('QBT PNR/Retrieve failed with Exception: ' . $exception->getMessage());
            if ($exception->hasResponse()) {
                $error = $exception->getResponse()->getBody()->getContents();
                Log::error('QBT PNR/Retrieve failed with Exception. Response: ' . $error);
                throw new \Exception("QBT PNR/Retrieve failed with Exception. Response: " . $error);
            } else {
                throw new \Exception("QBT PNR/Retrieve failed with Exception: " . $exception->getMessage());
            }
        }
        if ($response->getStatusCode() != 200) {
            Log::error("QBT PNR/Retrieve returned an error: " . $response->getReasonPhrase());
            throw new \Exception("QBT PNR/Retrieve returned an error: " . $response->getReasonPhrase());
        }

        return $response->getBody();
    }

    public static function sendSyncNotification($data)
    {
        $client = new Client();
        try {
            $response = $client->request('POST', env('QBTUrl') . self::SYNC_API, ['verify' => false, 'headers' => ['content-type' => 'application/json', 'Accept' => 'application/json'], 'body' => $data, 'auth' => [env('QBTUser'), env('QBTPassword')]]);
        } catch (\Exception $exception) {
            Log::error('QBT PNR/SyncHotelBooking failed with Exception: ' . $exception->getMessage());
            if ($exception->hasResponse()) {
                $error = $exception->getResponse()->getBody()->getContents();
                Log::error('QBT PNR/SyncHotelBooking failed with Exception. Response: ' . $error);
                throw new \Exception("QBT PNR/SyncHotelBooking failed with Exception. Response: " . $error);
            } else {
                throw new \Exception("QBT PNR/SyncHotelBooking failed with Exception: " . $exception->getMessage());
            }
        }

        if ($response->getStatusCode() != 200) {
            Log::error("QBT PNR/SyncHotelBooking returned an error: " . $response->getReasonPhrase());
            throw new \Exception("QBT PNR/SyncHotelBooking returned an error: " . $response->getReasonPhrase());
        }

        return $response->getBody();
    }

}