<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Log;
use App\Classes\QBTGateway\QBTGateway;
use App\Classes\Eway\EwayGateway;

class PaymentController extends Controller
{
    // The prefix for the credit card line
    const CC_PREFIX_UNUSED = "*ACEXFP-CC";
    const CC_PREFIX = "*ACEXFP-PRTYPE-HO";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param  string $id
     * @return Response
     */
    public function processPayment($id)
    {
        Log::debug('In processPayment');

        // Get the pnr
        $pnrDetails = null;
        try {
            // Get the pnr
            $requestData = '{
                    "Locator": "' . $id . '",
                    "PCC": "SYDQB2100"
                  }';
            $pnrDetails = QBTGateway::getPNRResponse($requestData);
            $pnrDetails = json_decode($pnrDetails);
        } catch (\Exception $exception) {
            return json_encode("{Error: " . $exception->getMessage() . "}");
        }

        $traveler = [];
        $firstName = $pnrDetails->Travellers[0]->FirstName;

        $traveler['fn'] = substr($firstName, 0, strrpos($firstName, ' ') + 1);
        $traveler['ln'] = $pnrDetails->Travellers[0]->Surname;

        $cc = [];

        foreach ($pnrDetails->Remarks as $remark) {
            if ($remark->RemarkType == 'RM') {
                if (strpos($remark->Text, self::CC_PREFIX) === 0) {
                    $splitUp = explode('-', $remark->Text);
                    $cc['number'] = $splitUp[4];
                    $cc['month'] = substr($splitUp[5], 0, 2);
                    $cc['year'] = substr($splitUp[5], 2, 2);

                    $splitUp[4] = 'XXXXXXXXXXXXXXXX';
                    $splitUp[5] = "XXXX";
                    $remark->Text = implode("-", $splitUp);
                }
                if (strpos($remark->Text, self::CC_PREFIX_UNUSED) === 0) {
                    $splitUp = explode('-', $remark->Text);
                    $splitUp[3] = 'XXXXXXXXXXXXXXXX';
                    $splitUp[4] = "XXXX";
                    $remark->Text = implode("-", $splitUp);
                }
            }
        }

        foreach ($pnrDetails->Itinerary as $itinerary) {
            foreach ($itinerary->Product->OptionalInfo as $optionalInfo) {
                if ($optionalInfo->Id == 'G') {
                    $optionalInfo->Detail = 'XXXXXXXXXXXXXXXX';
                }
            }
        }

        $customerData['Customer'] =
            ['FirstName' => $traveler['fn'], 'LastName' => $traveler['ln'],
             'CardDetails' => ['Name' => trim($traveler['fn']) . ' ' . trim($traveler['ln']), 'Number' => $cc['number'], 'ExpiryMonth' => $cc['month'], 'ExpiryYear' => $cc['year']]];
        try {
            $paymentToken = EwayGateway::createClient($customerData);
        } catch (\Exception $exception) {
            return json_encode("{Error: " . $exception->getMessage() . "}");
        }

        $imprint = base64_encode(substr($cc['number'], 5, 8));

        $result = json_encode(['Result' => ['Token' => $paymentToken, 'Imprint' => $imprint], 'Pnr' => $pnrDetails]);

        return $result;

    }

    /**
     * Fetch a Pnr and return data with PCI obfuscated.
     *
     * @param  string $id
     * @return Response
     */
    public function retrievePnr($id)
    {
        Log::debug('In retrievePnr');

        $pnrDetails = null;
        try {
            $requestData = '{
                    "Locator": "' . $id . '",
                    "PCC": "SYDQB2100"
                  }';
            $pnrDetails = QBTGateway::getPNRResponse($requestData);
            $pnrDetails = json_decode($pnrDetails);
        } catch (\Exception $exception) {
            return json_encode("{Error: " . $exception->getMessage() . "}");
        }

        $aotRefs = [];
        foreach ($pnrDetails->Itinerary as $itinerary) {
            $aotRefs[] = ['AOTRefConfirmed' => $itinerary->Product->AOTRefConfirmed, 'AOTRefOnRequest' => $itinerary->Product->AOTRefOnRequest];
        }

        foreach ($pnrDetails->Remarks as $remark) {
            if ($remark->RemarkType == 'RM') {
                if (strpos($remark->Text, self::CC_PREFIX) === 0) {
                    $splitUp = explode('-', $remark->Text);
                    $splitUp[4] = 'XXXXXXXXXXXXXXXX';
                    $splitUp[5] = "XXXX";
                    $remark->Text = implode("-", $splitUp);
                }
                if (strpos($remark->Text, self::CC_PREFIX_UNUSED) === 0) {
                    $splitUp = explode('-', $remark->Text);
                    $splitUp[3] = 'XXXXXXXXXXXXXXXX';
                    $splitUp[4] = "XXXX";
                    $remark->Text = implode("-", $splitUp);
                }
            }
        }

        foreach ($pnrDetails->Itinerary as $itinerary) {
            foreach ($itinerary->Product->OptionalInfo as $optionalInfo) {
                if ($optionalInfo->Id == 'G') {
                    $optionalInfo->Detail = 'XXXXXXXXXXXXXXXX';
                }
            }
        }

        $result = json_encode(['Result' => ['Pnr' => $pnrDetails]]);

        return $result;

    }

    /**
     * @param  string $id
     * @param string $seqments
     * @return Response
     */
    public function requestSync($id, $seqments)
    {
        Log::debug('In requestSync');
        $requestData = '{
            "Locator": "' . $id . '",
            "PCC": "SYDQB2104",
            "SegmentNumbers": [
              '. explode(',', $seqments) .'
            ]}';


        $pnrDetails = null;
        try {
            // Get the pnr
            $pnrDetails = QBTGateway::sendSyncNotification($requestData);

            $pnrDetails = json_decode($pnrDetails);
        } catch (\Exception $exception) {
            return json_encode("{Error: " . $exception->getMessage() . "}");
        }

        return "Requested sync for $id. Result: " . print_r($pnrDetails, true);

    }


}
