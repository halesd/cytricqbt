<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('qbtsync/{id}/{$seqments}', 'PaymentController@requestSync');

$router->get('qbtprocess/{id}', 'PaymentController@processPayment');

$router->get('qbtpnr/{id}', 'PaymentController@retrievePnr');


